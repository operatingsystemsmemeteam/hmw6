
// memory.c
//
// Tyler Manifold & Isaac Murillo
// CSCI40300
// HMW5
//

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "page_table.h"
#include "tlb.h"

#define MEM_SIZE (FRAME_SIZE * NUM_FRAMES)

typedef enum FRAME_STATE {FREE, NOT_FREE} frame_state;

FILE* addr_fp;
FILE* BCKSTR;

char* MEM;

int get_page(int);
int get_offset(int);
int frame_lookup(int);

int frames_loaded = 0;

int main()
{
	// init physical memory and free_frame
	MEM = (char*) malloc(MEM_SIZE);
	struct pt_node* page_table = pt_init();
	struct tlb_node* tlb = tlb_init();

	addr_fp = fopen("addresses.txt", "r");

	BCKSTR	= fopen("BACKING_STORE.bin", "rb");
	
	int n = 6;
	char* buf = malloc(n * sizeof buf);


	while (fgets(buf, n, addr_fp))
	{
		if (buf[0] != '\n')
		{
			int addr = atoi(buf);
			int page = get_page(addr);
			int offset = get_offset(addr);

			//printf("%5d: %-3d %d\n", addr, page, offset);

			int frame_number = frame_lookup(page);
			int physical = frame_number * PAGE_SIZE + offset;
			int value = MEM[physical];

			printf ("logical: %5d  physical: %5d  value: %4d\n", addr, physical, value);

		}
	}

	fclose(addr_fp);
	fclose(BCKSTR);

	return 0;
}

// return the frame number corresponding to page number p in page table. if PAGE_FAULT, read data into physical memory from BCKSTR and update page table.
int frame_lookup(int p)
{
	// first consult the TLB
	int frame = tlb_search(p);
	
	// if TLB-MISS, we must consult the page table
	if (frame < 0)
	{
		//printf ("%s: TLB_MISS. page: %d\n", __func__, p);

		frame = pt_search(p);

		// if page-MISS, we must read the data from BCKSTR and update the page table and the TLB
		if (frame < 0)
		{
			//printf ("%s: PAGE_FAULT. page: %d\n", __func__, p);

			fseek(BCKSTR, p * PAGE_SIZE, SEEK_SET);
			
			//printf("%s: i = %d\n", __func__, i);
			
			struct pt_node* temp;
			int f = 0;
			
			if (frames_loaded < NUM_FRAMES - 1)
			{
				frames_loaded++;
				f = frames_loaded;
			}
			else if (frames_loaded == NUM_FRAMES - 1) //  then no free frame was found, and we need to swap
			{
				//printf("%s: i = NUM_FRAMES | %d == %d\n", __func__, i, NUM_FRAMES);
				temp = pt_scan(); // scan through the table to find a victim
				p = temp->page;
				f = temp->frame;
				
				//printf("%s: p = %d, i = %d\n", __func__, p, i);
			}

			//pt_print();

			// read the 256-byte page into MEM at the i-th frame
			fread(&MEM[f * PAGE_SIZE], PAGE_SIZE, 1, BCKSTR);

			// update the page table to map logical address p to frame number i, and set valid-bit and reference-bit
			pt_update(p, f, 0x1, 0x1);

			frame = pt_search(p);
						
			//printf("%s: frame = %d\n", __func__, frame);
		}
	}

	// after the page table has been consulted, we must update the TLB with the enwly acquired frame
	tlb_prepend(p, frame);

	return frame;
}

int get_page(int address)
{
	return (address & 0xFF00) >> 8; // bitwise-AND the top 8 bits to get page number
}

int get_offset(int address)
{	
	return address & 0xFF; // bitwise-AND the bottom 8 bits to get offset within page
}
