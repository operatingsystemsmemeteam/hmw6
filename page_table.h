
// page_table.h
// Tyler Manifold & Isaac Murillo

#ifndef _PAGE_TABLE_H
#define _PAGE_TABLE_H

#include <stdlib.h>
#include <stdio.h>

#define PAGE_SIZE 256
#define NUM_PAGES 256
#define FRAME_SIZE 256
#define NUM_FRAMES 128


typedef struct pt_node {

	int page;
	int frame;
	char vi_bit;
	char ref_bit;

	struct pt_node* next;
} node;

struct pt_node* head;
struct pt_node* replace;

// create blank node and return a reference to it
struct pt_node* create_node() {
	struct pt_node* temp = (struct pt_node*) malloc(sizeof *temp);
	
	temp->vi_bit = 0x0;
	temp->ref_bit = 0x1;

	return temp;
}

// initialize the page table with NUM_PAGES entries
// return a reference to the head node
struct pt_node* pt_init()
{
	head = create_node();
	replace = create_node();

	struct pt_node* iter = head;
	
	replace = head;

	for (int i = 0; i < NUM_PAGES; i++)
	{
		//printf("p%d, vi:0x%X\n", i, iter->vi_bit); 
		iter->next = create_node();
		iter = iter->next;
	}

	return head;
}

// scan the page table for the next victim to replace, clearing reference bits along the way
// returns reference to victim node
struct pt_node* pt_scan()
{
	if (replace)
	{
		while (replace->ref_bit == 0x1)
		{
			replace->ref_bit = 0x0;
			//printf("%s: p = %d, f = %d. ref: 0x1 => 0x0\n", __func__, replace->page, replace->frame);
	
			if (replace->next)
				replace = replace->next;
			else
				replace = head;
		}
	
		//printf("%s: replace: p = %d, f = %d, ref = 0x%X\n\n", __func__, replace->page, replace->frame, replace->ref_bit);
	
	}
	
	return replace;
}

int pt_search(int page_number)
{
	struct pt_node* iter = head;

	int i = 0;

	while (i < page_number)
	{
		iter = iter->next;

		if (!iter)
		{
			printf ("%s: p%d DNE!\n", __func__, i);
			return -1;
		}

		i++;
	}

	if (i == page_number)
	{
		//printf ("%d == p%d\n", i, page_number);
		if (iter->vi_bit == 0x0)
		{
			//printf ("%s: page %d is empty. must fetch from BCKSTR.\n",__func__, page_number);
			return -1;
		}
		else if (iter->vi_bit == 0x1)
		{
			//printf("%s: frame corresponding to page %d found: %d\n",__func__, page_number, iter->frame);
			return iter->frame;
		}
	}
}

void pt_print()
{
	struct pt_node* iter = head;

	int i = 0;
	while (iter)
	{
		printf("p%d vi: 0x%X, ref:0x%X\n", i, iter->vi_bit, iter->ref_bit);
		iter = iter->next;
		i++;
	}
}

void pt_update(int p, int f, char vi, char ref)
{
	struct pt_node* iter = head;

	int i = 0;
	
	while (i < p)
	{
		iter = iter->next;
		i++;
	}

	if (i == p)
	{
		//printf ("%s: found node %d. frame = %d, vi-bit=0x%X\n", __func__, node, f, vi);
		iter->page = p;
		iter->frame = f;
		iter->vi_bit = vi;
		iter->ref_bit = ref;
	}
}

/*
int main()
{
	struct pt_node* table = pt_init();

	pt_print();
	pt_search(10);
}
*/
#endif // _PAGE_TABLE_H
