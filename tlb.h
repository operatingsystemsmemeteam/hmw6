// tlb.h
//
// Tyler Manifold & Isaac Murillo
// CSCI 40300
// HMW5

#ifndef _TLB_H_
#define _TLB_H_

#include <stdlib.h>
#include <stdio.h>

// the TLB will effectively function as a FIFO Queue
// This is implemented as a linked list featuring a reorder function that will be called
// when a node is accessed via search

#define TLB_SIZE 16

typedef struct tlb_node {

	int page_number;
	int frame_number;

	struct tlb_node* prev;
	struct tlb_node* next;

} tlb_node;
//TLB_NODE;

struct tlb_node* tlb_head;

int num_elements = 0;

// output the contents of the tlb
void tlb_print()
{
	struct tlb_node* iter = tlb_head;

	int i = 0;

	while (iter)
	{
		printf("%-3d %5d %5d\n", i, iter->page_number, iter->frame_number);
		iter = iter->next;
		i++;
	}
}

// create a new tlb_node and return a reference to it
struct tlb_node* create_tlb_node()
{
	
	//struct TLB_NODE* temp;
	//temp = (TLB_NODE*)malloc(sizeof(TLB_NODE));
	struct tlb_node* temp = (tlb_node*)malloc(sizeof(*temp));

	return temp;
}

// initialize tlb_head and return a reference to it
struct tlb_node* tlb_init()
{
	tlb_head = create_tlb_node();
	tlb_head->page_number = -1;
	tlb_head->next = NULL;

	return tlb_head;
}

// insert a node at the front with page number p and frame number f
void tlb_prepend(int p, int f)
{
	//printf("%s: prepending node.\n", __func__);
	
	if (tlb_head->page_number == -1)
	{
	//	printf ("%s: init head.\n", __func__);
		tlb_head->page_number = p;
		tlb_head->frame_number = f;
	}
	else
	{
	//	printf("%s: prepending new node.\n", __func__);
		struct tlb_node* new_node = create_tlb_node();

		new_node->page_number = p;
		new_node->frame_number = f;
		
		struct tlb_node* temp = tlb_head;
		temp->next = tlb_head->next;
		temp->prev = new_node;

		new_node->next = temp;
		tlb_head = new_node;
		
		//printf("%s: prepended node. p = %d, f = %d\n", __func__, p, f);
	}

	if (num_elements < TLB_SIZE)
	{
		num_elements++;
		//printf("%s: num_elements: %d\n", __func__, num_elements);
	}
	else if (num_elements == TLB_SIZE)
	{
		struct tlb_node* iter = tlb_head;
		
		//printf("%s: stepping through TLB.\n", __func__);
		while (iter->next->next)
		{
			iter = iter->next;
		}

		iter->next = NULL;
		
		//printf("%s: truncated tail node.\n", __func__);
	}
	
	//printf ("%s: finished prepending node.\n", __func__);
}

// push the specified node n to the top of the list, and shift everything else down.
void tlb_reorder(struct tlb_node* n)
{
	if (n->prev)
		n->prev->next = n->next;

	if (n->next)
		n->next->prev = n->prev;

	// move n to front
	tlb_prepend(n->page_number, n->frame_number);
}



// find page number p, return frame number or MISS
int tlb_search(int p)
{
	struct tlb_node* iter = tlb_head;

	int frame = -1;

	while (iter && iter->page_number != p)
	{
		iter = iter->next;
	}

	if (iter != NULL)
	{
		frame = iter->frame_number;

		//printf("%s: found frame %d. reordering.\n", __func__, frame);

		tlb_reorder(iter);
	}

	return frame;
}

/*
int main()
{
	struct tlb_node* TLB = tlb_init();

	for (int i = 0; i < 4; i++)
	{
		tlb_prepend(i, i*2);
	}

	tlb_print();

	int frame = tlb_search(1);

	printf("frame %d\n", frame);

	tlb_print();

	return 0;
}
*/
#endif // _TLB_H_
